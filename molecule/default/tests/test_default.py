import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('molecule_group')


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('molecule_group')


def test_api(host):
    cmd = host.run('curl  -H "Content-Type: application/json-rpc" -X POST  http://ics-ans-role-zabbix-host-web-apache-default/api_jsonrpc.php -d \'{"jsonrpc":"2.0","method":"apiinfo.version","id":1,"auth":null,"params":{}}\'')
    assert cmd.rc == 0
