# ics-ans-role-zabbix-host

Work in progress

Ansible role to add/delete host to a Zabbix server or proxy.

## Role Variables

```yaml
zabbix_host_server: http://ics-ans-role-zabbix-host-web-apache-default
zabbix_host_user: Admin
zabbix_host_pass: zabbix
zabbix_host_tag: red
zabbix_host_alias: ride
zabbix_host_informations: nothing
zabbix_host_location: here
zabbix_host_site_rack: rack1
zabbix_host_os: junos
zabbix_host_hardware: ex3400
zabbix_host_group: "Discovered hosts"
zabbix_host_link_template: "Template Module ICMP Ping"
# options are: agent, snmp, ipmi or jmx
zabbix_host_type: snmp
zabbix_host_ip_address: "{{ ansible_default_ipv4.address }}"
# 10050 for agent, 161 for snmp
zabbix_host_port: 161
zabbix_host_proxy:
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-zabbix-host
```

## License

BSD 2-clause
